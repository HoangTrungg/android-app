package com.example.chatapplication.model;

public class idrequestModel {
    private String id;
    private String accept_state;

    public idrequestModel(String id, String accept_state) {
        this.id = id;
        this.accept_state = accept_state;
    }

    public idrequestModel() {
    }

    public String getAccept_state() {
        return accept_state;
    }

    public void setAccept_state(String accept_state) {
        this.accept_state = accept_state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
