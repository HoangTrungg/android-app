package com.example.chatapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.function.Chats_Activity;
import com.example.chatapplication.model.chatModel;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private Context context;
    private List<userModel> users;
    private boolean ischat;
    private String lastmessage;
    private final String TAG = getClass().getSimpleName();

    public UserAdapter(Context context, List<userModel> users, boolean ischat) {
        this.context = context;
        this.users = users;
        this.ischat = ischat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_itemuser_offragmentuser, parent, false);
        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder viewHolder, int position) {
        final userModel user = users.get(position);
        Log.w(TAG, "position : " + users.get(position));
        viewHolder.tv_usernameprofile.setText(user.getUsername());
        if (user.getImageURL().equals("default")) {
            viewHolder.imgprofile.setImageResource(R.drawable.ngok);
        } else {
            Glide.with(context).load(user.getImageURL()).into(viewHolder.imgprofile);
        }
        if (ischat) {
            lastmessage(user.getId(), viewHolder.tv_lastmessage);
        } else {
            viewHolder.tv_lastmessage.setVisibility(View.GONE);
        }

        viewHolder.status_on.setVisibility(user.getStatus().equals("online") ? View.VISIBLE : View.GONE);
        viewHolder.status_off.setVisibility(!user.getStatus().equals("online") ? View.VISIBLE : View.GONE);

        viewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, Chats_Activity.class); // gan context thay cho acti hien thoi
            intent.putExtra("iduser", user.getId());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_usernameprofile, tv_lastmessage;

        public ImageView imgprofile;
        public ImageView status_on, status_off;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_usernameprofile = itemView.findViewById(R.id.tvusername);
            imgprofile = itemView.findViewById(R.id.circleimage_user);
            status_on = itemView.findViewById(R.id.status_user_on);
            status_off = itemView.findViewById(R.id.status_user_off);
            tv_lastmessage = itemView.findViewById(R.id.tv_lastmessage);
        }
    }

    private void lastmessage(String userid, TextView tv_lastmessage) {
        lastmessage = "default";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("chat");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatModel model = snapshot.getValue(chatModel.class);
                    if (model.getSender().equals(firebaseUser.getUid()) && model.getReceiver().equals(userid) ||
                            model.getSender().equals(userid) && model.getReceiver().equals(firebaseUser.getUid())) {
                        lastmessage = model.getMessage();
                    }
                }
//                Log.w("lastmessage22: " , "" + lastmessage);
                switch (lastmessage) {
                    case "default":
                        tv_lastmessage.setText("");
                        break;
                    default:
                        tv_lastmessage.setText(lastmessage);
                        break;

                }
                lastmessage = "default";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
