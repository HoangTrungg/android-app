package com.example.chatapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.function.Friend.Profile_user_search;
import com.example.chatapplication.model.userModel;

import java.util.List;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.ViewHolder> {
    private Context context;
    private List<userModel> usersearch;

    public SearchUserAdapter(Context context, List<userModel> users) {
        this.context = context;
        this.usersearch = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_itemuser_offragmentuser, parent, false);
        return new SearchUserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final userModel user = usersearch.get(position);
        viewHolder.tv_usernameprofile.setText(user.getUsername());
        if (user.getImageURL().equals("default")) {
            viewHolder.imgprofile.setImageResource(R.drawable.ngok);
        } else {
            Glide.with(context).load(user.getImageURL()).into(viewHolder.imgprofile);
        }
        viewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, Profile_user_search.class); // gan context thay cho acti hien thoi
            intent.putExtra("iduser", user.getId());
            Log.w("id of user click: ", "" + user.getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return usersearch.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_usernameprofile, tv_lastmessage;

        public ImageView imgprofile;
        public ImageView status_on, status_off;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_usernameprofile = itemView.findViewById(R.id.tvusername);
            imgprofile = itemView.findViewById(R.id.circleimage_user);
            status_on = itemView.findViewById(R.id.status_user_on);
            status_off = itemView.findViewById(R.id.status_user_off);
            tv_lastmessage = itemView.findViewById(R.id.tv_lastmessage);
        }
    }
}

