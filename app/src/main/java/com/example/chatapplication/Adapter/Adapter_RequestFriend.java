package com.example.chatapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.function.Chats_Activity;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_RequestFriend extends RecyclerView.Adapter<Adapter_RequestFriend.ViewHolder> {
    private Context context;
    private List<userModel> listrequestfriend;
    private RecyclerView view;
    private final String TAG = getClass().getSimpleName();

    public Adapter_RequestFriend(Context context, List<userModel> listrequestfriend, RecyclerView view) {
        this.context = context;
        this.listrequestfriend = listrequestfriend;
        this.view = view;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_request_friend, parent, false);
        return new Adapter_RequestFriend.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final userModel user = listrequestfriend.get(position);
        Log.w(TAG, "position : " + listrequestfriend.get(position));
        viewHolder.tv_usernameprofile.setText(user.getUsername());
        if (user.getImageURL().equals("default")) {
            viewHolder.circleImageView.setImageResource(R.drawable.ngok);
        } else {
            Glide.with(context).load(user.getImageURL()).into(viewHolder.circleImageView);
        }
        viewHolder.btn_Accept.setOnClickListener(v -> {
            addlistfriend(user.getId());
            listrequestfriend.remove(position);
            view.removeViewAt(position);
            Intent intent = new Intent(context, Chats_Activity.class);
            intent.putExtra("iduser", user.getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return listrequestfriend.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_usernameprofile;
        public CircleImageView circleImageView;
        public Button btn_Accept, btn_Tuchoi;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_usernameprofile = itemView.findViewById(R.id.tv_profilenameofrequest);
            circleImageView = itemView.findViewById(R.id.circleimage_profile);
            btn_Accept = itemView.findViewById(R.id.btnaccept);
            btn_Tuchoi = itemView.findViewById(R.id.btntuchoi);
        }
    }

    private void addlistfriend(String id) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("friend");
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        HashMap<String, Object> hashMap1 = new HashMap<>();
        hashMap1.put("accept_state", "ok");
        ref.child(firebaseUser.getUid()).child("listfriend").child(id).setValue(hashMap);
        ref.child(firebaseUser.getUid()).child("request_friend").child(id).updateChildren(hashMap1);
    }
}
