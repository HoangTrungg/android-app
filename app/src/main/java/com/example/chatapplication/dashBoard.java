package com.example.chatapplication;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.chatapplication.Fragment.FragmentChat;
import com.example.chatapplication.Fragment.FragmentProfile;
import com.example.chatapplication.Fragment.Fragment_User;
import com.example.chatapplication.function.ReceiveActivity;
import com.example.chatapplication.interfaces.OnDialogButtonClickListener;
import com.example.chatapplication.model.callModel;
import com.example.chatapplication.model.chatModel;
import com.example.chatapplication.model.userModel;
import com.example.chatapplication.services.userService;
import com.example.chatapplication.shared.dialogCreator;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class dashBoard extends AppCompatActivity implements OnDialogButtonClickListener {
    public static Boolean flagUserSignOut;
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    dialogCreator dc;
    userService us;
    private CircleImageView circleimageuser;
    private TextView tvuser;
    private Toolbar toolbar;
    private int[] getIcon = {
            R.drawable.ani_group,
            R.drawable.ani_chat,
            R.drawable.ani_user
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.requestFeature(Window.FEATURE_NO_TITLE | Window.FEATURE_ACTIVITY_TRANSITIONS);
            w.setEnterTransition(new Explode());
            w.setExitTransition(new Explode());
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        }
        setContentView(R.layout.activity_dash_board);
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Connecting");
        progress.setMessage("Please wait while we fetching data...");
        progress.show();
        Runnable progressRunnable = () -> progress.cancel();
        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);
        receiveCall();
        getView();
        bindEvent();
        createDialog();
    }

    @SuppressLint("ResourceAsColor")
    private void getView() {
        us = new userService(this, this);
        circleimageuser = findViewById(R.id.circleimage_user);
        tvuser = findViewById(R.id.tvusername);
        ViewPager viewPager = findViewById(R.id.viewpaper);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        flagUserSignOut = false;
        getSupportActionBar().setTitle("");
        dc = new dialogCreator(this);


        FragmentPagerAdapter pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return 3; // tao so luong fragment
            }

            @Override
            public Fragment getItem(int position) {
                Fragment fragment = null;
                switch (position) {
                    case 0:
                        fragment = new Fragment_User();
                        break;
                    case 1:
                        fragment = new FragmentChat();
                        break;
                    case 2:
                        fragment = new FragmentProfile();
                        break;
                }
                return fragment;
            }
        };
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tablayyout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        tab.setIcon(getIcon[tab.getPosition()]);
                        tabLayout.getTabAt(tab.getPosition()).getIcon().setColorFilter(R.color.colorPrimary, PorterDuff.Mode.SRC_IN);
                    }

                    //over here
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        final Drawable icon = tab.getIcon();
                        ((Animatable) icon).start();
                        tabLayout.getTabAt(tab.getPosition()).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    }
                });
        tabLayout.setTabTextColors(R.color.colorWhite, R.color.colorPrimary);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(getIcon[i]);
            tabLayout.getTabAt(i).getIcon().setColorFilter(R.color.colorPrimary, PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }
        checkandRequestPermission();
        checkuser_receive_notifi();
    }

    private void MainStartService() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Intent NotiService = new Intent(getApplicationContext(), notificationMessage.class);
        NotiService.putExtra("UserUID", firebaseUser.getUid());
        startService(NotiService);
    }

    private void checkuser_receive_notifi() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("chat");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatModel model = snapshot.getValue(chatModel.class);
                    if (!model.getSender().equals(firebaseUser.getUid())) {
                        MainStartService();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void createDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.this_is_us, null);
        Dialog dialog = dc.openDialog(view);
        int width = (int) (this.getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (this.getResources().getDisplayMetrics().heightPixels * 0.9);
        dialog.getWindow().setLayout(width, height);
        Button btnSu = view.findViewById(R.id.btnSu);
        btnSu.setOnClickListener(v -> dialog.dismiss());
        btnSu.postDelayed(() -> {
            btnSu.setVisibility(View.VISIBLE);
        }, 5000);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void bindEvent() {
        Intent intent = getIntent();
        String mId = intent.getStringExtra("id");
        us.validateUser(mId);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel user = dataSnapshot.getValue(userModel.class);
                tvuser.setText(user.getUsername());
                if (user.getImageURL().equals("default")) {
                    circleimageuser.setImageResource(R.drawable.ngok);
                } else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(circleimageuser);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                dc.openAlertDialog(this, "Do you want to sign out", "Yes", "No", this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void receiveCall() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("call").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                callModel call = dataSnapshot.getValue(callModel.class);
                if (!call.getReceiving().equals("nocall")) {
                    Intent intent = new Intent(dashBoard.this, ReceiveActivity.class);
                    startActivity(intent);
                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onPositiveButtonClicked() {
        flagUserSignOut = true;
        setUserOffline();
        FirebaseAuth.getInstance().signOut();
        finish();
    }

    @Override
    public void onNegativeButtonClicked() {
        Toast.makeText(this, "Canceled", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        dc.openAlertDialog(this, "Do you want to sign out", "Yes", "No", this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUserOnline();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserOnline();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!flagUserSignOut) {
            setUserOffline();
        } else {
            return;
        }
    }

    public static void setUserOffline() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase.getInstance().getReference().child("user/" + firebaseUser.getUid()).child("status").setValue("offline");
    }

    public static void setUserOnline() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase.getInstance().getReference().child("user/" + firebaseUser.getUid()).child("status").setValue("online");
    }

    private void checkandRequestPermission() {
        String[] permisson = new String[]{
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };
        List<String> listpermisson = new ArrayList<>();
        for (String permiss : permisson) {
            if (ContextCompat.checkSelfPermission(this, permiss) != PackageManager.PERMISSION_GRANTED) {
                listpermisson.add(permiss);
            }
        }
        if (!listpermisson.isEmpty()) {
            ActivityCompat.requestPermissions(this, listpermisson.toArray(new String[listpermisson.size()]), 1);
        }
    }
}
