package com.example.chatapplication.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapplication.Adapter.Adapter_RequestFriend;
import com.example.chatapplication.Adapter.UserAdapter;
import com.example.chatapplication.R;
import com.example.chatapplication.model.chatlist;
import com.example.chatapplication.model.idrequestModel;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class FragmentChat extends Fragment {
    private static final String key_fragment = "key_frag";
    private RecyclerView recyclerView;
    private TextView tv_fr;
    private Adapter_RequestFriend adapter_requestFriend;
    private UserAdapter adapteruser;
    private ArrayList<userModel> listrchat123;
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    List<chatlist> listchat;
    private final String TAG = getClass().getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_chat, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_fragmentchat);
        tv_fr = view.findViewById(R.id.tvfriend);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        //listrequestfriend = new ArrayList<>();
        listrchat123 = new ArrayList<>();


        //readuser();
        readchat();
        return view;
    }

    private void readchat() {
        listchat = new ArrayList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("listchat").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        chatlist chat = snapshot.getValue(chatlist.class);
                        if (!chat.getId().equals(firebaseUser.getUid())) {
                            listchat.add(chat);
                        }

                    }
                    read();
                } else {
                    tv_fr.setText("Hay nhan tin cho ban be");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void read() {
        listrchat123.clear();
        for (int i = 0; i < listchat.size(); i++) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user").child(listchat.get(i).getId());
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    userModel model = dataSnapshot.getValue(userModel.class);
                    listrchat123.add(model);
                    adapteruser = new UserAdapter(getContext(), listrchat123, true);
                    recyclerView.setAdapter(adapteruser);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void readuser() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("friend");

        reference.child(firebaseUser.getUid()).child("request_friend").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //listrequestfriend.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        idrequestModel idrequest = snapshot.getValue(idrequestModel.class);
                        Log.w(TAG, "idofuser_reuqest: " + idrequest.getId());
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user").child(idrequest.getId());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                userModel model = dataSnapshot.getValue(userModel.class);
                                if (!model.getId().equals(idrequest.getId())) {
                                    recyclerView.setVisibility(View.GONE);
                                    tv_fr.setVisibility(View.VISIBLE);
                                    tv_fr.setText("Ooops Let's add friends");
                                } else {
                                    //listrequestfriend.add(model);
                                    // adapter_requestFriend = new Adapter_RequestFriend(getContext(), listrequestfriend, recyclerView);
                                    recyclerView.setAdapter(adapter_requestFriend);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                } else {
                    tv_fr.setText("Opp Let's add friends");
                    recyclerView.setVisibility(View.GONE);
                    tv_fr.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static FragmentChat newInstances(int idfrag) {
        FragmentChat fragment = new FragmentChat();
        Bundle args = new Bundle();
        args.putInt(key_fragment, idfrag);
        fragment.setArguments(args);
        return fragment;
    }

}
