package com.example.chatapplication.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapplication.Adapter.SearchUserAdapter;
import com.example.chatapplication.Adapter.UserAdapter;
import com.example.chatapplication.R;
import com.example.chatapplication.model.listfriendModel;
import com.example.chatapplication.model.userModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Fragment_User extends Fragment {
    private static final String key_fragment = "key_frag";
    private RecyclerView recyclerView, recyclerView_search;
    private AutoCompleteTextView act_search;
    private ImageView img_search;
    private FloatingActionButton fl_search, fl_endsearch;
    private UserAdapter userAdapter;
    private SearchUserAdapter searchUserAdapter;
    private List<userModel> fusers;
    private List<userModel> usersearch;
    private List<userModel> userlist;
    private ArrayList<listfriendModel> listfr;
    private DatabaseReference ref;
    private ArrayList<String> arrten;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__user, container, false);
        recyclerView_search = view.findViewById(R.id.recyclerview_search);
        act_search = view.findViewById(R.id.act_search_user);
        img_search = view.findViewById(R.id.img_search_user);
        fl_endsearch = view.findViewById(R.id.search_fd_endseach);
        fl_search = view.findViewById(R.id.search_fbtn);
        recyclerView = view.findViewById(R.id.recyclerview_user);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView_search = view.findViewById(R.id.recyclerview_search);
        recyclerView_search.setHasFixedSize(true);
        recyclerView_search.setLayoutManager(new LinearLayoutManager(getContext()));
        userlist = new ArrayList<>();
        fusers = new ArrayList<>();
        listfr = new ArrayList<>();
        // Inflate the layout for this fragment;
        readuser();
        viewsearch();
        addEvent();
        return view;
    }

    @SuppressLint("RestrictedApi")
    private void addEvent() {
        fl_search.setOnClickListener(v -> {
            fl_endsearch.setVisibility(View.VISIBLE);
            fl_search.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            recyclerView_search.setVisibility(View.VISIBLE);
            act_search.setVisibility(View.VISIBLE);
            img_search.setVisibility(View.VISIBLE);
        });
        fl_endsearch.setOnClickListener(v -> {
            fl_endsearch.setVisibility(View.GONE);
            fl_search.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView_search.setVisibility(View.GONE);
            act_search.setVisibility(View.GONE);
            img_search.setVisibility(View.GONE);
        });
        img_search.setOnClickListener(v -> {
            if (Check() >= 0) {
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user");
                usersearch = new ArrayList<>();
                usersearch.clear();
                reference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            userModel user = snapshot.getValue(userModel.class);
                            if (!user.getId().equals(firebaseUser.getUid())) {
                                userlist.add(user);
                                Log.w("Userlist : ", "" + userlist);
                                if (user.getId().equals(userlist.get(Check()).getId())) {
                                    usersearch.add(user);
                                }
                            }
                        }
                        searchUserAdapter = new SearchUserAdapter(getContext(), usersearch);
                        recyclerView_search.setAdapter(searchUserAdapter);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            } else {
                Toast.makeText(getContext(), "ko thay", Toast.LENGTH_SHORT).show();
            }

        });
    }

    int Check() {
        for (int i = 0; i < arrten.size(); i++) {
            if (act_search.getText().toString().equals(arrten.get(i).toString()))
                return i;
        }
        return -1;
    }

    private void viewsearch() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            ref = FirebaseDatabase.getInstance().getReference();
            arrten = new ArrayList<String>();
            arrten.clear();

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user");
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        userModel userModel1 = snapshot.getValue(com.example.chatapplication.model.userModel.class);
                        if (!userModel1.getId().equals(firebaseUser.getUid())) {
                            if (userModel1.getUsername() != null){
                                arrten.add(new String(userModel1.getUsername()));
                                ArrayAdapter<String> adapterten = new ArrayAdapter<String>(Objects.requireNonNull(getContext()), R.layout.support_simple_spinner_dropdown_item, arrten);
                                act_search.setAdapter(adapterten);
                                act_search.setThreshold(1);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void readuser() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        ref = FirebaseDatabase.getInstance().getReference("friend");
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                fusers.clear();
                userlist.clear();
                listfr.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userModel friend = snapshot.getValue(userModel.class);
                    assert friend != null;
                    assert firebaseUser != null;
                    if (!friend.getId().equals(firebaseUser.getUid())) {
                        fusers.add(friend);
                        userlist.add(friend);
                    }
                }
                userAdapter = new UserAdapter(getContext(), fusers, true);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public static Fragment_User newInstances(int idfrag) {
        Fragment_User fragment = new Fragment_User();
        Bundle args = new Bundle();
        args.putInt(key_fragment, idfrag);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {

        super.onStart();
        usersearch = new ArrayList<>();
        arrten = new ArrayList<>();
        usersearch.clear();
        recyclerView_search.removeAllViewsInLayout();
        arrten.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        usersearch = new ArrayList<>();
        arrten = new ArrayList<>();
        usersearch.clear();
        arrten.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        usersearch = new ArrayList<>();
        arrten = new ArrayList<>();
        usersearch.clear();
        arrten.clear();
    }
}
