package com.example.chatapplication.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.dashBoard;
import com.example.chatapplication.interfaces.OnDialogButtonClickListener;
import com.example.chatapplication.model.userModel;
import com.example.chatapplication.services.cryptoService;
import com.example.chatapplication.shared.dialogCreator;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class FragmentProfile extends Fragment implements OnDialogButtonClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private Boolean flag;
    Button btnchangepassword;
    EditText etusername;
    ImageView imgView_editProfile, imgView_editUsername, imgView_editV, imgView_editX;
    private static final String key_fragment = "key_frag";
    private CircleImageView imageprofile;
    private TextView tv_nameprofile;
    private static final int IMAGE_REQUEST = 1;
    private Uri imageuri;
    private StorageTask uploadtask;
    StorageReference storageReference;
    DatabaseReference reference;
    FirebaseUser firebaseUser;
    LinearLayout layout;
    private int AnimationDuration;
    dialogCreator dc;
    cryptoService cs;
    Context mContext;


    public static FragmentProfile newInstance(int idfrag) {
        FragmentProfile fragment = new FragmentProfile();
        Bundle args = new Bundle();
        args.putInt(key_fragment, idfrag);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_profile, container, false);
        imageprofile = view.findViewById(R.id.circleimage_profile);
        tv_nameprofile = view.findViewById(R.id.tv_profilename);
        btnchangepassword = view.findViewById(R.id.btnProfileChangePassword);
        etusername = view.findViewById(R.id.etProfileUsername);
        imgView_editProfile = view.findViewById(R.id.imgView_editProfile);
        imgView_editUsername = view.findViewById(R.id.imgView_editUsername);
        imgView_editV = view.findViewById(R.id.imgView_editV);
        imgView_editX = view.findViewById(R.id.imgView_editX);
        layout = view.findViewById(R.id.ProfileLayout);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
        storageReference = FirebaseStorage.getInstance().getReference("Upload_Images");
        flag = false;
        AnimationDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
        imageprofile.setOnClickListener(v -> openImages());
        imgView_editProfile.setOnClickListener(v -> editProfile());
        imgView_editUsername.setOnClickListener(v -> changeUsername());
        imgView_editV.setOnClickListener(v -> changeUsername());
        imgView_editX.setOnClickListener(v -> cancelChangeUsername());
        btnchangepassword.setOnClickListener(v -> changeUserPassword());
        setUpProfile();
        isPasswordChangeable();
        dc = new dialogCreator(view.getContext());
        mContext = view.getContext();
        cs = new cryptoService();
        return view;
    }

    private void setUpProfile() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel user = dataSnapshot.getValue(userModel.class);
                firebaseUser.getIdToken(true);
//                if (!firebaseUser.getPhoneNumber().isEmpty()) {
//                } else if (firebaseUser.isEmailVerified()) {
//                } else {
//                }
                tv_nameprofile.setText(user.getUsername());
                etusername.setText(user.getUsername());
                if (user.getImageURL().equals("default")) {
                    imageprofile.setImageResource(R.drawable.ngok);
                } else {
                    Glide.with(getContext()).load(user.getImageURL()).into(imageprofile);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void changeUsername() {
        if (!flag) {
            imgView_editUsername.setVisibility(View.GONE);
            etusername.setEnabled(true);
            showView(imgView_editV);
            showView(imgView_editX);
            flag = true;
        } else {
            if (etusername.getText().length() > 6) {
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userModel user = dataSnapshot.getValue(userModel.class);
                        FirebaseDatabase.getInstance().getReference().child("user/" + user.getId()).child("username").setValue(etusername.getText().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
                cancelChangeUsername();
            } else {
                etusername.setError("Username must have at least 6 character");
                cancelChangeUsername();
            }
            flag = false;
        }
    }

    private void cancelChangeUsername() {
        etusername.setEnabled(false);
        imgView_editV.setVisibility(View.GONE);
        imgView_editX.setVisibility(View.GONE);
        setUpProfile();
        showView(imgView_editUsername);
        flag = false;
    }

    private void openImages() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    private void editProfile() {
        if (layout.getVisibility() == View.GONE) {
            showView(layout);
        } else {
            hideView(layout);
        }
    }

    public void hideView(View view) {
        view.animate()
                .alpha(0f)
                .setDuration(AnimationDuration)
                .setListener(null);

        view.setVisibility(View.GONE);
    }

    public void showView(View view) {
        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.animate()
                .alpha(1f)
                .setDuration(AnimationDuration)
                .setListener(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageuri = data.getData();
            if (uploadtask != null && uploadtask.isInProgress()) {
                Toast.makeText(getContext(), "Upload in progress", Toast.LENGTH_SHORT).show();
            } else
                UploadImage();
        }
    }

    private String getFileExtension(Uri uri) { // lay duoi uri
        ContentResolver resolver = getContext().getContentResolver();
        MimeTypeMap typeMap = MimeTypeMap.getSingleton();
        return typeMap.getExtensionFromMimeType(resolver.getType(uri));
    }

    private void UploadImage() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Đang tải ảnh đẹp lên ahihi");
        progressDialog.show();
        if (imageuri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imageuri));
            // lay duoi timesystem + duoi uri  day len storage
            uploadtask = fileReference.putFile(imageuri); // put anh + dinh dang uri len
            uploadtask.continueWithTask((Continuation<UploadTask.TaskSnapshot, Task<Uri>>) task -> {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return fileReference.getDownloadUrl();
            }).addOnCompleteListener((OnCompleteListener<Uri>) task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String muri = downloadUri.toString();
                    reference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("imageURL", muri);
                    reference.updateChildren(map);
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(getContext(), "Loi", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(e -> {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            });
        } else {
            Toast.makeText(getContext(), "No image chose", Toast.LENGTH_SHORT).show();
        }
    }

    private void isPasswordChangeable() {
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser.getIdToken(true)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult().getSignInProvider().equals("password")) {
                            btnchangepassword.setVisibility(View.VISIBLE);
                        } else {
                            btnchangepassword.setVisibility(View.GONE);
                        }
                    } else {
                        // Handle error -> task.getException();
                    }
                });
    }

    private void changeUserPassword() {
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.dialog_change_password, null);
        Dialog dialog = dc.openDialog(view);
        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (mContext.getResources().getDisplayMetrics().heightPixels * 0.4);
        dialog.getWindow().setLayout(width, height);
        EditText etoldpassword = view.findViewById(R.id.etChangePasswordOldPassword);
        EditText etnewpassword = view.findViewById(R.id.etChangePasswordNewPassword);
        EditText etconfirmpassword = view.findViewById(R.id.etChangePasswordConfirmPassword);
        Button btnconfirm = view.findViewById(R.id.btnChangePasswordConfirm);
        Button btncancel = view.findViewById(R.id.btnChangePasswordCancel);
        btnconfirm.setOnClickListener(v -> {
            if (confirmChangeUserPassword(etoldpassword, etnewpassword, etconfirmpassword))
                dialog.dismiss();
        });
        btncancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private boolean confirmChangeUserPassword(EditText oldpass, EditText newpass, EditText repass) {
        Boolean flag = true;
        if (oldpass.getText().toString().isEmpty()) {
            oldpass.setError("This field is empty");
            flag = false;
        } else {
            oldpass.setError(null);
            flag = true;
        }
        if (newpass.getText().toString().isEmpty()) {
            newpass.setError("This field is empty");
            flag = false;
        } else {
            newpass.setError(null);
            flag = true;
        }
        if (repass.getText().toString().isEmpty()) {
            repass.setError("This field is empty");
            flag = false;
        } else {
            repass.setError(null);
            flag = true;
        }
        if (repass.getText().toString().equals(newpass.getText().toString()) && !repass.getText().toString().isEmpty()) {
            flag = true;
            String hash = "", oldhash = "";
            try {
                hash = cs.getSHA256Hash(newpass.getText().toString().trim());
                oldhash = cs.getSHA256Hash(oldpass.getText().toString().trim());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            AuthCredential credential = EmailAuthProvider.getCredential(firebaseUser.getEmail(), oldhash);
            String finalHash = hash;
            firebaseUser.reauthenticate(credential)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            firebaseUser.updatePassword(finalHash)
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            dc.openAlertDialog(mContext, "Password changed", "Confirm", null, this);
                                        }
                                    });
                        } else {
                            Toast.makeText(mContext, "Old Password Incorrect", Toast.LENGTH_LONG).show();
                        }
                    });
//
        } else {
            flag = false;
            repass.setError("Please make sure your passwords match");
        }
        return flag;
    }

    @Override
    public void onPositiveButtonClicked() {
        dashBoard.setUserOffline();
        FirebaseAuth.getInstance().signOut();
        dashBoard.flagUserSignOut = true;
        getActivity().finish();
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
