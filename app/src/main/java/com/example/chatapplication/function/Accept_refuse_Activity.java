package com.example.chatapplication.function;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.model.callModel;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Accept_refuse_Activity extends AppCompatActivity {
    private ImageView img_accept_call, img_cancel_call, img_usercall;
    private FrameLayout layout_usercalling;
    private TextView tv_usercalling;
    private String idrecev;
    private DatabaseReference reference;
    private FirebaseUser firebaseUser;
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_refuse_);

        addControls();
        addEvents();
    }

    private void addControls() {
        img_accept_call = findViewById(R.id.img_acceptcall);
        img_cancel_call = findViewById(R.id.img_cancelcall);
        img_usercall = findViewById(R.id.img_usercall);
        layout_usercalling = findViewById(R.id.layout_usercalling);
        tv_usercalling = findViewById(R.id.tv_usercalling);
        setimg_forfr();
    }

    private void setimg_forfr() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("call").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                callModel call = dataSnapshot.getValue(callModel.class);
                idrecev = call.getReceiving();
                if (!call.getReceiving().equals("nocall")) {
                    ref = FirebaseDatabase.getInstance().getReference("user").child(idrecev);
                    ref.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            userModel user = dataSnapshot.getValue(userModel.class);
                            if (user.getImageURL().equals("default")) {
                                img_usercall.setImageResource(R.drawable.ngok);
                            } else {
                                Glide.with(getApplicationContext()).load(user.getImageURL()).into(img_usercall);


                            }
                            tv_usercalling.setText(user.getUsername().toString() + " Đang muốn liên hệ với bạn ...");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {
                    finish();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addEvents() {
        img_cancel_call.setOnClickListener(v -> {
            updatecall();
            finish();
        });
        img_accept_call.setOnClickListener(v -> updateaccept());
    }

    private void updateaccept() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("call").child(firebaseUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("accept", "ok");
        reference.updateChildren(hashMap);
        Intent intent = new Intent(getApplicationContext(), ReceiveActivity.class);
        startActivity(intent);
    }

    private void updatecall() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("call").child(firebaseUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("receiving", "nocall");
        reference.updateChildren(hashMap);
    }
}
