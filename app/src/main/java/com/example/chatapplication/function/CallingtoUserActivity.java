package com.example.chatapplication.function;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.model.callModel;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class CallingtoUserActivity extends AppCompatActivity {
    private TextView tv_userreceive;
    private ImageView img_userreceive, img_endcall;
    private String userid;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callingto_user);
        Intent intent = getIntent();
        userid = intent.getStringExtra("receiveruser");
        checkcall();
        receivestate(userid);
        addControls();
        addEvents();
    }

    private void receivestate(String userid) {
        reference = FirebaseDatabase.getInstance().getReference("call").child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                callModel call = dataSnapshot.getValue(callModel.class);
                if (call.getAccept().equals("ok")) {
                    Intent inte = new Intent(getApplicationContext(), CallActivity.class);
                    inte.putExtra("idreceiver", userid);
                    startActivity(inte);
                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addControls() {
        tv_userreceive = findViewById(R.id.tv_userreceive);
        img_userreceive = findViewById(R.id.img_userreceive);
        img_endcall = findViewById(R.id.img_end_call);

        setinfo(userid);

    }

    private void checkcall() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("call").child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                callModel call = dataSnapshot.getValue(callModel.class);
                if (call.getReceiving().equals("nocall")) {
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setinfo(String userid) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user").child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel user = dataSnapshot.getValue(userModel.class);
                if (user.getImageURL().equals("default")) {
                    img_userreceive.setImageResource(R.drawable.ngok);
                } else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(img_userreceive);
                }
                tv_userreceive.setText("Đang liên hệ tới " + user.getUsername());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void addEvents() {
        img_endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setsatecalloffgetuid();
                setsatecalloffuser();
                finish();
            }
        });
    }

    private void setsatecalloffuser() {
        reference = FirebaseDatabase.getInstance().getReference("call").child(userid);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("receiving", "nocall");
        reference.updateChildren(hashMap);
    }

    private void setsatecalloffgetuid() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("call").child(firebaseUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("calling", "nocall");
        reference.updateChildren(hashMap);
    }
}
