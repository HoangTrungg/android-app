package com.example.chatapplication.function.Friend;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.chatapplication.R;
import com.example.chatapplication.dashBoard;
import com.example.chatapplication.model.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile_user_search extends AppCompatActivity {
    private Intent intent;
    private CircleImageView circleImageView;
    private TextView tv_username;
    private Button btn_addfriend, btn_deny;
    private String iduser;
    private DatabaseReference reference;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user_search);
        addControls();
        loaduser();
        addEvents();

    }

    private void loaduser() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user");
        ref.child(iduser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel model = dataSnapshot.getValue(userModel.class);
                if (model.getImageURL().equals("default")) {
                    circleImageView.setImageResource(R.drawable.ngok);
                } else {
                    Glide.with(getApplicationContext()).load(model.getImageURL()).into(circleImageView);
                }
                tv_username.setText(model.getUsername().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addControls() {
        circleImageView = findViewById(R.id.circleimage_profile);
        tv_username = findViewById(R.id.tv_profilename);
        btn_addfriend = findViewById(R.id.btnaddfriend);
        btn_deny = findViewById(R.id.btndeny);
        intent = getIntent();
        iduser = intent.getStringExtra("iduser");
    }

    private void addEvents() {
        btn_addfriend.setOnClickListener(v -> {
            addfriend();
            Intent intent = new Intent(getApplicationContext(), dashBoard.class);
            startActivity(intent);
        });
    }

    private void addfriend() {
        reference = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", firebaseUser.getUid());
        hashMap.put("accept_state", "nope");
        reference.child("friend").child(iduser).child("request_friend").child(firebaseUser.getUid()).setValue(hashMap);
    }
}
