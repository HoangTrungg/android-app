package com.example.chatapplication;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.chatapplication.function.Chats_Activity;
import com.example.chatapplication.model.chatModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.app.Notification.DEFAULT_ALL;

public class notificationMessage extends Service {
    private DatabaseReference reference;
    private NotificationCompat.Builder notifiBuilder;
    private String userid;
    private Bitmap largeIcon;
    private static final int MY_NOTIFICATION_ID = 12345;
    private static final int MY_REQUEST_CODE = 100;
    private String lastmessage;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        reference = FirebaseDatabase.getInstance().getReference("chat");
        notifiBuilder = new NotificationCompat.Builder(notificationMessage.this);
        CreateBitmap();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        userid = intent.getStringExtra("UserUID");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatModel model = snapshot.getValue(com.example.chatapplication.model.chatModel.class);
                    lastmessage = model.getMessage();
                    Log.w("Lastmessage : ", "" + lastmessage);
                }
                notifiBuilder.setAutoCancel(true);
                notifiBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
                notifiBuilder.setSmallIcon(R.mipmap.ic_launcher);
                notifiBuilder.setDefaults(DEFAULT_ALL);
                notifiBuilder.setPriority(Notification.PRIORITY_MAX);
                notifiBuilder.setLargeIcon(largeIcon);
                notifiBuilder.setTicker("This is a ticker");
                notifiBuilder.setWhen(System.currentTimeMillis() + 10 * 1000);
                notifiBuilder.setContentTitle("Sender's info: ");
                notifiBuilder.setContentText(lastmessage);
                Intent intent = new Intent(getApplicationContext(), Chats_Activity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), MY_REQUEST_CODE,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notifiBuilder.setContentIntent(pendingIntent);
                NotificationManager notificationService = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                Notification notification = notifiBuilder.build();
                notificationService.notify(MY_NOTIFICATION_ID, notification);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return startId;
    }

    void CreateBitmap() {
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.noti_icon);
        Bitmap b = bitmapdraw.getBitmap();
        largeIcon = Bitmap.createScaledBitmap(b, width, height, false);
    }

    @Override
    public void onDestroy() {
        reference = null;
        notifiBuilder = null;
        super.stopSelf();
        super.onDestroy();
    }
}
